//we should give access of private variables to others.so,we using this modular pattern
 var myPattern =
 (
     function(){
         var a;
         var b;
         var c;

         function Result1(A,B){
          a=A;
          b=B;
         }
          function Result2(){
          alert(a + b);
          }
         
         return {
             pattern: Result1,
             hello:Result2,
         };
     }
 )();